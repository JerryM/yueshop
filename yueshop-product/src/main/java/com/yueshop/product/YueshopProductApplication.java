package com.yueshop.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YueshopProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(YueshopProductApplication.class, args);
    }

}
